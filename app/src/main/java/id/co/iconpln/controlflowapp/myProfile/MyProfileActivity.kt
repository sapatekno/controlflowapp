package id.co.iconpln.controlflowapp.myProfile

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myProfile.ProfileLoginResponse
import id.co.iconpln.controlflowapp.model.myProfile.ProfileResponse
import id.co.iconpln.controlflowapp.model.myProfile.ProfileUser
import id.co.iconpln.controlflowapp.myProfileLogin.MyProfileLoginActivity
import kotlinx.android.synthetic.main.activity_my_profile.*

class MyProfileActivity : AppCompatActivity(), View.OnClickListener {
    companion object {
        const val REQUEST_CODE = 200
    }

    private lateinit var viewModel: MyProfileActivityViewModel

    private var profileLoginResponse: ProfileLoginResponse? = null

    private lateinit var profileUserPreference: ProfileUserPreference
    private lateinit var profileUser: ProfileUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_profile)

        changeTitle()
        initViewModel()
        setButtonClickListener()
        showExistingPreferences()
    }

    private fun showExistingPreferences() {
        profileUserPreference = ProfileUserPreference(this)
        profileUser = profileUserPreference.getProfileUser()

        val token = profileUserPreference.getProfileUser().userToken

        if (!token.isNullOrEmpty()) {
            initiateGetProfile(token)
        } else {
            showLogoutProfile()
        }
    }

    private fun initiateGetProfile(token: String) {
        pbProfileLoading.visibility = View.VISIBLE
        llProfileContent.visibility = View.GONE
        fetchUserProfile(token)
    }

    private fun showLogoutProfile() {
        pbProfileLoading.visibility = View.GONE
        llProfileContent.visibility = View.VISIBLE
        tvProfileWarning.visibility = View.VISIBLE
        btnProfileToLogin.visibility = View.VISIBLE
        btnProfileToLogout.visibility = View.GONE

        tvProfileId.text = resources.getString(R.string.empty)
        tvProfileName.text = resources.getString(R.string.empty)
        tvProfileEmail.text = resources.getString(R.string.empty)
        tvProfileHandphone.text = resources.getString(R.string.empty)
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(
            MyProfileActivityViewModel::class.java
        )
    }

    private fun changeTitle() {
        supportActionBar?.title = "My Profile"
    }

    private fun setButtonClickListener() {
        btnProfileToLogin.setOnClickListener(this)
        btnProfileToLogout.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnProfileToLogin -> {
                val myProfileLoginIntent = Intent(this, MyProfileLoginActivity::class.java)
                startActivityForResult(myProfileLoginIntent, REQUEST_CODE)
            }
            R.id.btnProfileToLogout -> {
                Toast.makeText(this, "Logout Clicked", Toast.LENGTH_SHORT).show()
                profileUserPreference.removeProfileUser(profileUser)
                showLogoutProfile()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE) {
            if (resultCode == MyProfileLoginActivity.RESULT_CODE) {
                profileLoginResponse =
                    data?.getParcelableExtra(MyProfileLoginActivity.EXTRA_PROFILE_RESULT) as ProfileLoginResponse
                saveProfileUserPreference()
            }
        }
    }

    private fun saveProfileUserPreference() {
        if (profileUserPreference != null) {
            profileUser.userToken = profileLoginResponse?.token
            profileUserPreference.setProfileUser(profileUser)
            Toast.makeText(this, "Token has been saved at Shared preferences", Toast.LENGTH_SHORT)
                .show()
            showExistingPreferences()
        }
    }

    private fun fetchUserProfile(token: String) {
        viewModel.getProfile(token).observe(this, Observer { profileResponse ->
            if (profileResponse != null) {
                showProfile(profileResponse)
            } else {
                Toast.makeText(this, "Failed to get profile", Toast.LENGTH_SHORT).show()
                profileUserPreference.removeProfileUser(profileUser)
                showLogoutProfile()
            }
        })
    }

    private fun showProfile(profileResponse: ProfileResponse) {
        pbProfileLoading.visibility = View.GONE
        llProfileContent.visibility = View.VISIBLE
        tvProfileWarning.visibility = View.GONE
        btnProfileToLogin.visibility = View.GONE
        btnProfileToLogout.visibility = View.VISIBLE

        tvProfileId.text = profileResponse.id.toString()
        tvProfileName.text = profileResponse.name
        tvProfileEmail.text = profileResponse.email
        tvProfileHandphone.text = profileResponse.phone
    }
}
