package id.co.iconpln.controlflowapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.co.iconpln.controlflowapp.model.Person
import kotlinx.android.synthetic.main.activity_intent_object.*

class IntentObjectActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_PERSON = "extra_person"
    }

    private lateinit var person: Person

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_object)

        getIntentExtras()
        showData()
    }

    private fun getIntentExtras() {
        person = intent.getParcelableExtra(EXTRA_PERSON)
    }

    private fun showData() {
        val text =
            "Nama : ${person.name} \nAge : ${person.age} \nEmail : ${person.email} \n City : ${person.city}"
        tev_object_data.text = text
    }
}
