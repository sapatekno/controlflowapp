package id.co.iconpln.controlflowapp

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import id.co.iconpln.controlflowapp.viewModel.OperationViewModel
import kotlinx.android.synthetic.main.activity_operation.*
import java.text.DecimalFormat

class OperationActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var btnAdd: Button
    private lateinit var btnSubstract: Button
    private lateinit var btnMultiply: Button
    private lateinit var btnDivide: Button
    private lateinit var btnReset: Button
    private lateinit var edtInputX: EditText
    private lateinit var edtInputY: EditText
    private lateinit var tevOperator: TextView
    private lateinit var tevResult: TextView

    private var inputX: Double = 0.0
    private var inputY: Double = 0.0

    private lateinit var operationViewModel: OperationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_operation)

        "Calculator App".setTitleApps()
        setInitView()

        setInitModel()
        setButtonClickListener()

        displayResult()
    }

    private fun displayResult() {
        tevOperator.text = operationViewModel.operationOperator
        tevResult.text = setFormat(operationViewModel.operationResult)
    }

    private fun setInitModel() {
        operationViewModel = ViewModelProviders.of(this)[OperationViewModel::class.java]
    }

    private fun setInitView() {
        btnAdd = operation_btn_op_add
        btnSubstract = operation_btn_op_substract
        btnMultiply = operation_btn_op_multiply
        btnDivide = operation_btn_op_divide
        btnReset = operation_btn_reset
        tevOperator = operation_tev_operator
        tevResult = operation_tev_result
        edtInputX = operation_edt_bilangan_x
        edtInputY = operation_edt_bilangan_y
    }

    private fun String.setTitleApps() {
        val actionBar = supportActionBar
        actionBar?.title = this
    }

    private fun getInputNumbers() {
        inputX = if (edtInputX.text.isNotEmpty()) {
            edtInputX.text.toString().toDouble()
        } else {
            0.0
        }

        inputY = if (edtInputY.text.isNotEmpty()) {
            edtInputY.text.toString().toDouble()
        } else {
            0.0
        }
    }

    private fun setButtonClickListener() {
        btnAdd.setOnClickListener(this)
        btnSubstract.setOnClickListener(this)
        btnMultiply.setOnClickListener(this)
        btnDivide.setOnClickListener(this)
        btnReset.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.operation_btn_op_add -> {
                val process = {
                    getInputNumbers()
                    val operation = Operation.Add(inputX)
                    val operator = getString(R.string.operation_tev_operator_add)
                    calculate(operation, operator, inputY)
                }
                operationProcess(process)
            }
            R.id.operation_btn_op_substract -> {
                val process = {
                    getInputNumbers()
                    val operation = Operation.Substract(inputX)
                    val operator = getString(R.string.operation_tev_operator_substract)
                    calculate(operation, operator, inputY)
                }
                operationProcess(process)
            }
            R.id.operation_btn_op_multiply -> {
                val process = {
                    getInputNumbers()
                    val operation = Operation.Multiply(inputX)
                    val operator = getString(R.string.operation_tev_operator_multiply)
                    calculate(operation, operator, inputY)
                }
                operationProcess(process)
            }
            R.id.operation_btn_op_divide -> {
                val process = {
                    getInputNumbers()

                    if (inputY == 0.0) {
                        tevResult.text =
                            getString(R.string.operation_error_divided_by_zero_text)
                    } else {
                        val operation = Operation.Divide(inputX)
                        val operator = getString(R.string.operation_tev_operator_divide)
                        calculate(operation, operator, inputY)
                    }
                }
                operationProcess(process)
            }
            R.id.operation_btn_reset -> {
                resetApp()
            }
        }
    }

    private fun operationProcess(process: () -> Unit) {
        when {
            edtInputX.text.isEmpty() -> {
                tevResult.text = getString(R.string.operation_error_input_x)
                edtInputX.requestFocus()
            }
            edtInputY.text.isEmpty() -> {
                tevResult.text = getString(R.string.operation_error_input_y)
                edtInputY.requestFocus()
            }
            else -> {
                process()
            }
        }
    }

    private fun calculate(operation: Operation, operator: String, y: Double) {
        operationViewModel.execute(y,operation)
        operationViewModel.operationOperator = operator
        displayResult()
    }

    private fun resetApp() {
        edtInputX.setText("0")
        edtInputY.setText("0")
        operationViewModel.operationOperator = ""
        operationViewModel.operationResult = 0.0
        displayResult()
        edtInputX.requestFocus()
    }

    private fun setFormat(data: Double): String {
        val format = DecimalFormat("###.##")
        return format.format(data)
    }
}
