package id.co.iconpln.controlflowapp

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import id.co.iconpln.controlflowapp.viewModel.VolumeViewModel
import kotlinx.android.synthetic.main.activity_volume.*
import java.text.DecimalFormat

class VolumeActivity : AppCompatActivity(), View.OnClickListener {

    private val edtLength: EditText get() = edt_volume_length
    private val edtWidth: EditText get() = edt_volume_width
    private val edtHeight: EditText get() = edt_volume_height
    private val tevResult: TextView get() = tev_volume_result
    private val btnCalculate: Button get() = btn_volume_calculate

    private var length: String = ""
    private var width: String = ""
    private var height: String = ""

    private lateinit var volumeViewModel: VolumeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_volume)

        getString(R.string.volume_app_title_text).setActionBarTitle()
        initViewModel()
        setButtonListener()
        displayResult()
    }

    private fun initViewModel() {
        volumeViewModel = ViewModelProviders.of(this).get(VolumeViewModel::class.java)
    }

    private fun displayResult() {
        tevResult.text = setFormat(volumeViewModel.result)
    }

    private fun setButtonListener() {
        btnCalculate.setOnClickListener(this)
    }

    private fun String.setActionBarTitle() {
        val actionBar = supportActionBar
        actionBar?.title = this
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btn_volume_calculate -> {
                length = edtLength.text.toString()
                width = edtWidth.text.toString()
                height = edtHeight.text.toString()

                when {
                    length.isEmpty() -> {
                        edtLength.error = getString(R.string.error_empty_text)
                        edtLength.requestFocus()
                    }
                    width.isEmpty() -> {
                        edtWidth.error = getString(R.string.error_empty_text)
                        edtWidth.requestFocus()
                    }
                    height.isEmpty() -> {
                        edtHeight.error = getString(R.string.error_empty_text)
                        edtHeight.requestFocus()
                    }
                    else -> {
                        volumeViewModel.doCalculate(length, width, height)
                        displayResult()
                    }
                }
            }
        }
    }

    private fun setFormat(data: Double): String {
        val format = DecimalFormat("###.##")
        return format.format(data)
    }

}
