package id.co.iconpln.controlflowapp.hero

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.Hero
import id.co.iconpln.controlflowapp.model.HeroesData
import kotlinx.android.synthetic.main.activity_grid_hero.*

class GridHeroActivity : AppCompatActivity() {

    private var gridHero: ArrayList<Hero> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_hero)

        setupGridHero()
        showRecyclerGrid()
    }

    private fun setupGridHero() {
        rvGridHero.setHasFixedSize(true)
        gridHero.addAll(HeroesData.listDataHero)

        setupGridDivider()
    }

    private fun setupGridDivider() {
        val dividerItemDecoration =
            DividerItemDecoration(rvGridHero.context, DividerItemDecoration.VERTICAL)
        rvGridHero.addItemDecoration(dividerItemDecoration)
    }

    private fun showRecyclerGrid() {
        rvGridHero.layoutManager = GridLayoutManager(this, 2)
        val griHeroAdapter = GridHeroAdapter(gridHero)
        rvGridHero.adapter = griHeroAdapter

        griHeroAdapter.setOnItemClickCallBack(object : GridHeroAdapter.OnItemClickCallback {
            override fun onItemClick(hero: Hero) {
                Toast.makeText(this@GridHeroActivity, hero.name, Toast.LENGTH_SHORT).show()
            }
        })
    }
}
