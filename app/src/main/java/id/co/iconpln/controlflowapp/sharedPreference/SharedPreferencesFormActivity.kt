package id.co.iconpln.controlflowapp.sharedPreference

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.isDigitsOnly
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_shared_preferences_form.*

class SharedPreferencesFormActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_TYPE_FORM = "extra_type_form"
        const val EXTRA_RESULT = "extra_result"
        const val EXTRA_USER = "extra_user"
        const val RESULT_CODE = 101
        const val TYPE_ADD = 1
        const val TYPE_EDIT = 2
    }

    private lateinit var user: User
    private var formType: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shared_preferences_form)
        setButtonClickListener()
        getIntentExtra()
        setupForm("", "")
        setupFormType()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setupFormType() {
        when (formType) {
            TYPE_ADD -> {
                setupForm("Tambah Baru", "Simpan")
            }
            TYPE_EDIT -> {
                setupForm("Ubah", "Update")
                showPreferenceInForm()
            }
        }
    }

    private fun showPreferenceInForm() {
        etPrefFromName.setText(user.name)
        etPrefFromEmail.setText(user.email)
        etPrefFromAge.setText(user.age.toString())
        etPrefFromHp.setText(user.handphone)
        rbPrefFormReading.isChecked = user.hasReadingHobby
        rbPrefFormNotReading.isChecked = !user.hasReadingHobby
    }

    private fun getIntentExtra() {
        user = intent.getParcelableExtra(EXTRA_USER) as User
        formType = intent.getIntExtra(EXTRA_TYPE_FORM, 0)
    }

    private fun setupForm(actionBarTitle: String, btnTitle: String) {
        supportActionBar?.title = actionBarTitle
        btnPrefFormSave.text = btnTitle
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) finish()
        return super.onOptionsItemSelected(item)
    }

    private fun setButtonClickListener() {
        btnPrefFormSave.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnPrefFormSave -> {
                val name = etPrefFromName.text.toString().trim()
                val email = etPrefFromEmail.text.toString().trim()
                val age = etPrefFromAge.text.toString().trim()
                val handphone = etPrefFromHp.text.toString().trim()
                val hasReadingHobby = rgPrefFormHobby.checkedRadioButtonId == R.id.rbPrefFormReading

                if (name.isEmpty()) {
                    etPrefFromName.error = resources.getString(R.string.sp_field_required)
                    return
                }
                if (!isValidEMail(email)) {
                    etPrefFromEmail.error = resources.getString(R.string.sp_field_email_not_valid)
                    return
                }
                if (handphone.isEmpty()) {
                    etPrefFromHp.error = resources.getString(R.string.sp_field_required)
                    return
                }
                if (!handphone.isDigitsOnly()) {
                    etPrefFromHp.error = resources.getString(R.string.sp_field_digit_only)
                    return
                }
                if (age.isEmpty()) {
                    etPrefFromAge.error = resources.getString(R.string.sp_field_required)
                    return
                }

                Toast.makeText(
                    this,
                    "Data: $name, $email, $age, $handphone, $hasReadingHobby",
                    Toast.LENGTH_SHORT
                ).show()

                saveUser(name, email, age, handphone, hasReadingHobby)
                val resultsIntent = Intent().putExtra(EXTRA_RESULT, user)
                setResult(RESULT_CODE, resultsIntent)
                finish()
            }
        }
    }

    private fun isValidEMail(email: CharSequence): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun saveUser(
        name: String,
        email: String,
        age: String,
        handphone: String,
        hasReadingHobby: Boolean
    ) {
        val userPreference = UserPreference(this)
        user.name = name
        user.email = email
        user.age = Integer.parseInt(age)
        user.handphone = handphone
        user.hasReadingHobby = hasReadingHobby
        userPreference.setUser(user)
        Toast.makeText(this, "Data Saved", Toast.LENGTH_SHORT).show()
    }
}
