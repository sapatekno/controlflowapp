package id.co.iconpln.controlflowapp.bottomSheetDialog

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_bottom_sheet.*
import kotlinx.android.synthetic.main.fragment_bottom_sheet.*
import kotlinx.android.synthetic.main.layout_bottom_sheet.*
import kotlinx.android.synthetic.main.layout_content_main.*

class BottomSheetActivity : AppCompatActivity(), View.OnClickListener,
    BottomSheetFragment.ItemCLickListener {

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_sheet)

        setupActionBar()
        setButtonClickListener()
        setupBottomSheetBehavior()
    }

    private fun setupBottomSheetBehavior() {
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet)
        bottomSheetBehavior.setBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        btnBottomSheet.text = getString(R.string.expand_bottom_sheet)
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        btnBottomSheet.text = getString(R.string.close_bottom_sheet)
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }
        })
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbarBottomSheet)
    }

    private fun setButtonClickListener() {
        btnBottomSheet.setOnClickListener(this)
        btnBottomSheetDialog.setOnClickListener(this)
        btnBottomSheetDialogFragment.setOnClickListener(this)
        btnBottomPayment.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnBottomSheet -> {
                if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                    btnBottomSheet.text = getString(R.string.close_bottom_sheet)
                } else {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                    btnBottomSheet.text = getString(R.string.expand_bottom_sheet)
                }
            }
            R.id.btnBottomSheetDialog -> {
                val dialogView = layoutInflater.inflate(R.layout.fragment_bottom_sheet, null)
                val bottomSheetDialog = BottomSheetDialog(this)
                bottomSheetDialog.setContentView(dialogView)

                bottomSheetDialog.show()
                setDialogClickListener(bottomSheetDialog)
            }
            R.id.btnBottomSheetDialogFragment -> {
                val bottomSheetFragment = BottomSheetFragment()
                bottomSheetFragment.show(supportFragmentManager, bottomSheetFragment.tag)
            }
            R.id.btnBottomPayment -> {
                tvBottomActivity.text = getString(R.string.payment_success)
            }
        }
    }

    private fun setDialogClickListener(bottomSheetDialog: BottomSheetDialog) {
        bottomSheetDialog.llBottomPreview.setOnClickListener {
            showDialogText(bottomSheetDialog.tvBottomPreview.text.toString())
        }
        bottomSheetDialog.llBottomShare.setOnClickListener {
            showDialogText(bottomSheetDialog.tvBottomShare.text.toString())
        }
        bottomSheetDialog.llBottomEdit.setOnClickListener {
            showDialogText(bottomSheetDialog.tvBottomEdit.text.toString())
        }
        bottomSheetDialog.llBottomSearch.setOnClickListener {
            showDialogText(bottomSheetDialog.tvBottomSearch.text.toString())
        }
        bottomSheetDialog.llBottomExit.setOnClickListener {
            showDialogText(bottomSheetDialog.tvBottomExit.text.toString())
        }
    }

    private fun showDialogText(text: String) {
        onItemClick("Dialog $text")
    }

    override fun onItemClick(text: String) {
        tvBottomActivity.text = text
    }
}
