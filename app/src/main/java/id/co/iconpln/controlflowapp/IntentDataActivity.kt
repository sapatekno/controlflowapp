package id.co.iconpln.controlflowapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_intent_data.*

class IntentDataActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_NAME = "extra_name"
        const val EXTRA_AGE = "extra_age"
    }

    private var name: String = ""
    private var age: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_data)

        getIntentExtra()
        showData()
    }

    private fun getIntentExtra() {
        name = intent.getStringExtra(EXTRA_NAME)
        age = intent.getIntExtra(EXTRA_AGE, 0)
    }

    private fun showData() {
        val text = "Nama saya $name dan umur saya $age tahun"
        tev_data_received.text = text
    }
}
