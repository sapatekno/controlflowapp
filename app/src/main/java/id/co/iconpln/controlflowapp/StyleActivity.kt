package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class StyleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_style)

        setActionBarTitle()
        addBackButton()
    }

    private fun addBackButton() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setActionBarTitle() {
        val actionBar = supportActionBar
        actionBar?.title = getString(R.string.style_google_pixel_text)
    }
}
