package id.co.iconpln.controlflowapp.myUserForm

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.loopj.android.http.AsyncHttpClient.log
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteUser
import id.co.iconpln.controlflowapp.database.FavoriteViewModel
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import kotlinx.android.synthetic.main.activity_my_user_form.*

class MyUserFormActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var user: UserDataResponse
    private lateinit var myUserFormViewModel: MyUserFormViewModel
    private lateinit var favoriteViewModel: FavoriteViewModel
    private var userId: Int? = null
    private var isEditUser = false

    private var isFavorite: Boolean = false
    private var menuItem: Menu? = null

    private var favoriteUserId: Long? = null

    companion object {
        // const val EXTRA_USER = "extra_user"
        const val EXTRA_USER_EDIT = "extra_user_edit"
        const val EXTRA_USER_ID = "extra_user_id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user_form)

        initViewModel()
        initIntentExtra()
        setButtonClickListener()
        checkForm(isEditUser)
    }

    private fun checkForm(editUser: Boolean) {
        if (editUser) {
            // populateFormData(user)
            fetchUserData()
        } else {
            btnUserFormAdd.visibility = View.VISIBLE
            btnUserFormSave.visibility = View.GONE
            btnUserFormDelete.visibility = View.GONE
        }
    }

    private fun initViewModel() {
        myUserFormViewModel = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(MyUserFormViewModel::class.java)

        favoriteViewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(
                FavoriteViewModel::class.java
            )
    }

    private fun populateFormData(user: UserDataResponse) {
        etUserFormName.setText(user.name)
        etUserFormAddress.setText(user.address)
        etUserFormHp.setText(user.phone)

        btnUserFormSave.visibility = View.VISIBLE
        btnUserFormDelete.visibility = View.VISIBLE
        btnUserFormAdd.visibility = View.GONE
    }

    private fun fetchUserData() {
        pbMyUserFormLoading.visibility = View.VISIBLE
        llMyUserForm.visibility = View.GONE
        getUser(userId as Int)
    }

    private fun initIntentExtra() {
        //        user = if (intent.hasExtra(EXTRA_USER)) {
        //            intent.getParcelableExtra(EXTRA_USER)
        //        } else {
        //            UserDataResponse("", 0, "", "")
        //        }

        userId = intent.getIntExtra(EXTRA_USER_ID, 0)
        isEditUser = intent.getBooleanExtra(EXTRA_USER_EDIT, false)
    }

    private fun setButtonClickListener() {
        btnUserFormSave.setOnClickListener(this)
        btnUserFormDelete.setOnClickListener(this)
        btnUserFormAdd.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnUserFormSave -> {
                val updateUserData = UserDataResponse(
                    etUserFormAddress.text.toString(),
                    userId ?: 0,
                    etUserFormName.text.toString(),
                    etUserFormHp.text.toString()
                )

                if (userId != null) {
                    updateUser(userId as Int, updateUserData)
                }
            }
            R.id.btnUserFormDelete -> {
                if (userId != null) {
                    deleteUser(userId as Int)
                }
            }
            R.id.btnUserFormAdd -> {
                userId = 0
                val createUserData = UserDataResponse(
                    etUserFormAddress.text.toString(),
                    userId ?: 0,
                    etUserFormName.text.toString(),
                    etUserFormHp.text.toString()
                )
                createUser(createUserData)
            }
        }
    }

    private fun createUser(userData: UserDataResponse) {
        myUserFormViewModel.createUser(userData).observe(
            this, Observer { userDataResponse ->
                if (userDataResponse != null) {
                    Toast.makeText(this, "User Create Successfully", Toast.LENGTH_SHORT).show()
                    finish()
                } else {
                    Toast.makeText(this, "Failed to Create User", Toast.LENGTH_SHORT).show()
                }
            }
        )
    }

    private fun updateUser(id: Int, userData: UserDataResponse) {
        myUserFormViewModel.updateUser(id, userData).observe(
            this, Observer { userDataResponse ->
                if (userDataResponse != null) {
                    Toast.makeText(this, "User Update Succesfully", Toast.LENGTH_SHORT).show()
                    updateFavoriteUser(userDataResponse)
                    finish()
                } else {
                    Toast.makeText(this, "Failed to Update User", Toast.LENGTH_SHORT).show()
                }
            }
        )
    }

    private fun updateFavoriteUser(userDataResponse: UserDataResponse) {
        if (favoriteUserId != null) {
            favoriteViewModel.updateUser(
                FavoriteUser(
                    favoriteUserId as Long,
                    userDataResponse.address,
                    userDataResponse.id.toString(),
                    userDataResponse.name,
                    userDataResponse.phone
                )
            )
        }

    }

    private fun deleteUser(id: Int) {
        myUserFormViewModel.deleteUser(id).observe(
            this, Observer { userDataResponse ->
                if (userDataResponse != null) {
                    Toast.makeText(this, "User Delete Succesfully", Toast.LENGTH_SHORT).show()
                    if (isFavorite) removeFromFavorite()
                    finish()
                } else {
                    Toast.makeText(this, "Failed to Delete User", Toast.LENGTH_SHORT).show()
                }
            }
        )
    }

    private fun getUser(userId: Int) {
        myUserFormViewModel.getUser(userId).observe(
            this, Observer { userDataResponse ->
                if (userDataResponse != null) {
                    Toast.makeText(this, "User Loaded Successfully!", Toast.LENGTH_SHORT).show()
                    populateFormData(userDataResponse)
                    Toast.makeText(this, "User Loaded Successfully!", Toast.LENGTH_SHORT).show()
                    llMyUserForm.visibility = View.VISIBLE
                    pbMyUserFormLoading.visibility = View.GONE
                    setFavorite()
                } else {
                    Toast.makeText(this, "Failed to Load User!", Toast.LENGTH_SHORT).show()
                    pbMyUserFormLoading.visibility = View.GONE
                }
            }
        )
    }

    private fun setFavorite() {
        if (userId != null) {
            favoriteViewModel.getUser(userId as Int).observe(this, Observer { favoriteUser ->
                log.d("favdatabase", "getUser $favoriteUser")
                isFavorite = favoriteUser != null
                setFavoriteIcon()

                if (favoriteUser != null) {
                    favoriteUserId = favoriteUser.favUserId
                }
            })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_my_user_form, menu)
        menuItem = menu
        setFavoriteIcon()

        if (!isEditUser) {
            menu.findItem(R.id.action_favorite).isVisible = false
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_favorite -> {

                if (llMyUserForm.isGone) {
                    Toast.makeText(this, "Cant add to Favorite", Toast.LENGTH_SHORT).show()
                    return false
                }

                isFavorite = !isFavorite
                setFavoriteIcon()
                addOrRemoveFavorite()
                true
            }
            else -> true
        }
    }

    private fun setFavoriteIcon() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite)
        else {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_unfavorite)
        }
    }

    private fun addOrRemoveFavorite() {
        if (isFavorite) {
            addToFavorite()
        } else {
            removeFromFavorite()
        }

        favoriteViewModel.getAllFavoriteUsers().observe(this, Observer { listFavUser ->
            if (listFavUser.isNotEmpty()) {
                for (i in 0 until listFavUser.size) {
                    log.d(
                        "favdatabase",
                        "" + listFavUser[i].favUserId + " : " + listFavUser[i].userName
                    )
                }
            }
        })
    }

    private fun removeFromFavorite() {
        if (userId != null) {
            favoriteViewModel.deleteUser(userId as Int)
        }
        favoriteUserId = null
    }

    private fun addToFavorite() {
        favoriteViewModel.insertUser(
            FavoriteUser(
                0,
                etUserFormAddress.text.toString(),
                userId.toString(),
                etUserFormName.text.toString(),
                etUserFormHp.text.toString()
            )
        )
    }

}
