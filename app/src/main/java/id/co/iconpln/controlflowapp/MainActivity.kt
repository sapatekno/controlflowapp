package id.co.iconpln.controlflowapp

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.co.iconpln.controlflowapp.utils.setActionBarTitle
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.pow

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getString(R.string.hitung_pangkat_text).setActionBarTitle(supportActionBar)
        start()
        setOnClickListener()
    }


    private fun start() {
        etNilai.setText("0")
        etNilai.requestFocus()
    }

    private fun setOnClickListener() {
        btnShow.setOnClickListener {
            if (etNilai.text!!.isNotEmpty()) {
                val number = etNilai.text.toString().toDouble()
                hitungPangkat(number)
            }
        }
    }

    private fun hitungPangkat(nilai: Double) {
        val hasil = nilai.pow(2)
        val output = getString(R.string.main_result_text) + hasil
        tvHasil.text = output
    }

}
