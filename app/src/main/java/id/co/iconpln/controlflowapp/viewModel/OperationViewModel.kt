package id.co.iconpln.controlflowapp.viewModel

import androidx.lifecycle.ViewModel
import id.co.iconpln.controlflowapp.Operation

class OperationViewModel : ViewModel() {
    var operationResult: Double = 0.0
    var operationOperator: String = ""

    fun execute(x: Double, operation: Operation) {
        operationResult = when (operation) {
            is Operation.Add -> operation.value + x
            is Operation.Divide -> operation.value / x
            is Operation.Multiply -> operation.value * x
            is Operation.Substract -> operation.value - x
        }
    }

}
