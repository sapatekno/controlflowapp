package id.co.iconpln.controlflowapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_intent_result.*

class IntentResultActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        val TAG: String = this.javaClass.simpleName
        const val EXTRA_VALUE = "extra_value"
        const val RESULT_CODE = 110
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_result)

        setButtonClickListener()
    }

    private fun setButtonClickListener() {
        btn_result_choose.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_result_choose -> {
                when {
                    rag_result_number.checkedRadioButtonId != 0 -> {
                        var value = 0
                        when (rag_result_number.checkedRadioButtonId) {
                            R.id.rab_result_50 -> value = 50
                            R.id.rab_result_100 -> value = 100
                            R.id.rab_result_150 -> value = 150
                            R.id.rab_result_200 -> value = 200
                        }
                        Log.d(TAG, "Value : $value")

                        val resultIntent = Intent()
                        resultIntent.putExtra(EXTRA_VALUE, value)
                        setResult(RESULT_CODE, resultIntent)
                        finish()
                    }
                }
            }
        }
    }
}
