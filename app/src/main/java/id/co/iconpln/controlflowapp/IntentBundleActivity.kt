package id.co.iconpln.controlflowapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_intent_bundle.*

class IntentBundleActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_BUNDLE_NAME = "extra_bundle_name"
        const val EXTRA_BUNDLE_AGE = "extra_bundle_age"
    }

    private var name = ""
    private var age = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_bundle)

        getIntentExtras()
        showData()
    }

    private fun getIntentExtras() {
        name = intent.extras?.getString(EXTRA_BUNDLE_NAME) ?: ""
        age = intent.extras?.getInt(EXTRA_BUNDLE_AGE) ?: 0
    }

    private fun showData() {
        val text = "Nama saya $name dan umur saya $age tahun"
        tev_bundle_data.text = text
    }
}
