package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_classification.*

class ClassificationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_classification)

        "Klasifikasi Nilai".setActionBarTitle()
        etClassificationNilai.requestFocus()
        etClassificationNilai.setText("0")
        btnClassificationShow.setOnClickListener {
            if (etClassificationNilai.text.isNotEmpty()) {
                val nilai = etClassificationNilai.text.toString().toInt()
                doClassification(nilai)
            }
        }

    }

    private fun doClassification(nilai: Int) {
        val hasil: String

        when (nilai) {
            in 0..70 -> {
                hasil = "Maaf anda belum Lulus!"
            }
            in 71..80 -> {
                hasil = "Sudah Lulus tapi belum lulus banget!"
            }
            in 81..100 -> {
                hasil = "Sudah Lulus Sempurna!"
            }
            !in 0..1000 -> {
                hasil = "Data tidak boleh lebih dari 1000"
                Toast.makeText(this, hasil, Toast.LENGTH_SHORT).show()
            }
            else -> {
                hasil = "Inputnya error"
            }
        }

        tvClassifiactionHasil.text = hasil
    }

    private fun String.setActionBarTitle() {
        val actionBar = supportActionBar
        actionBar!!.title = this
    }

}
