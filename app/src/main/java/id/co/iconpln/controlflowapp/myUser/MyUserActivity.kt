package id.co.iconpln.controlflowapp.myUser

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import id.co.iconpln.controlflowapp.myUserFavorite.MyUserFavoriteActivity
import id.co.iconpln.controlflowapp.myUserForm.MyUserFormActivity
import kotlinx.android.synthetic.main.activity_my_user.*

class MyUserActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var adapter: MyUserAdapter
    private lateinit var myUserViewModel: MyUserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user)

        supportActionBar?.title = "My User"

        initViewModel()
        showListUser()
        fetchUserData()
        setButtonClickListener()
        addListClickListener()
    }

    override fun onResume() {
        super.onResume()
        fetchUserData()
    }
    private fun setButtonClickListener() {
        fabMyUserAdd.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.fabMyUserAdd -> {
                Toast.makeText(this, "Button clicked", Toast.LENGTH_SHORT).show()
                val myUserFormIntent = Intent(this, MyUserFormActivity::class.java)
                startActivity(myUserFormIntent)
            }
        }
    }

    private fun addListClickListener() {
        adapter.setOnItemClickCallback(object : MyUserAdapter.OnItemClickCallback {
            override fun onItemClick(myUser: UserDataResponse) {
                Toast.makeText(this@MyUserActivity, "You choose ${myUser.name}", Toast.LENGTH_SHORT)
                    .show()
                openUserForm(myUser)
            }
        })
    }

    private fun openUserForm(myUser: UserDataResponse) {
        val userFormIntent = Intent(this, MyUserFormActivity::class.java)
        // userFormIntent.putExtra(MyUserFormActivity.EXTRA_USER, myUser)
        userFormIntent.putExtra(MyUserFormActivity.EXTRA_USER_ID, myUser.id)
        userFormIntent.putExtra(MyUserFormActivity.EXTRA_USER_EDIT, true)
        startActivity(userFormIntent)
    }

    private fun fetchUserData() {
        // get value from View Model's Live Data
        showLoading(true)
        myUserViewModel.getListUser().observe(this, Observer { userItem ->
            if (userItem != null) {
                adapter.setData(userItem)
                showLoading(false)
            }
        })
    }

    private fun initViewModel() {
        myUserViewModel = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(MyUserViewModel::class.java)
    }

    private fun showListUser() {
        adapter = MyUserAdapter()
        adapter.notifyDataSetChanged()

        rvMyUserList.layoutManager = LinearLayoutManager(this)
        rvMyUserList.adapter = adapter
    }

    private fun showLoading(state: Boolean) {
        if (state) {
            pbMyUserFormLoading.visibility = View.VISIBLE
        } else {
            pbMyUserFormLoading.visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_my_user, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_favorite_list -> {
                val myUserFavoriteIntent = Intent(this, MyUserFavoriteActivity::class.java)
                startActivity(myUserFavoriteIntent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
