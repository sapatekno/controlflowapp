package id.co.iconpln.controlflowapp.myProfile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.co.iconpln.controlflowapp.model.myProfile.ProfileResponse
import id.co.iconpln.controlflowapp.network.MyProfileNetwotkRepository

class MyProfileActivityViewModel : ViewModel() {
    fun getProfile(token: String): MutableLiveData<ProfileResponse> {
        return MyProfileNetwotkRepository().getProfile(token)
    }
}