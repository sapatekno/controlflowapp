package id.co.iconpln.controlflowapp.thread

import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_backgroud_thread.*
import kotlinx.coroutines.*
import java.lang.Runnable
import java.lang.ref.WeakReference
import java.net.URL

class BackgroudThreadActivity : AppCompatActivity(), View.OnClickListener,
    ContactAsyncTaskCallBack {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_backgroud_thread)

        setButtonClickListener()
    }

    private fun setButtonClickListener() {
        btnThreadWorker.setOnClickListener(this)
        btnThreadHandler.setOnClickListener(this)
        btnThreadAsyncTask.setOnClickListener(this)
        btnThreadCoroutine.setOnClickListener(this)
        btnThreadCoroutineAsync.setOnClickListener(this)
    }

    suspend fun getNumber(): Int {
        delay(1000)
        return 3 * 2
    }

    suspend fun getContact(): String {

        return withContext(Dispatchers.IO) {
            URL("https://api.androidhive.info/contacts").readText()
        }
    }

    private val contactHandler = Handler { msg: Message ->
        tvThreadHandlerResult.text = msg.obj as String
        true
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnThreadWorker -> {
                /*
                Don't Call Network in Main thread

                val ContactResultText = URL("https://api.androidhive.info/contacts").readText()
                tvThreadWorkerResult.text = ContactResultText
                */

                Thread(Runnable {
                    val contactResultText = URL("https://api.androidhive.info/contacts").readText()
                    // Don't Call UI Thread in Background
                    // tvThreadWorkerResult.text = contactResultText

                    tvThreadWorkerResult.post {
                        tvThreadWorkerResult.text = contactResultText
                    }
                }).start()
            }
            R.id.btnThreadHandler -> {
                Thread(Runnable {
                    val contactResultText = URL("https://api.androidhive.info/contacts").readText()
                    val msg = Message.obtain()
                    msg.obj = contactResultText
                    msg.target = contactHandler
                    msg.sendToTarget()
                }).start()
            }
            R.id.btnThreadAsyncTask -> {
                val contactUrl = URL("https://api.androidhive.info/contacts")
                FetchContactAsyncTask(this).execute(contactUrl)
            }
            R.id.btnThreadCoroutine -> {
                runBlocking {
                    launch {
                        delay(1000)
                        tvThreadCoroutineResult.text = "Coroutine"
                    }
                }
            }
            R.id.btnThreadCoroutineAsync -> {
                runBlocking {
                    // val numberAsync = async { getNumber() }
                    // val result = numberAsync.await()

                    val contactAsync = async { getContact() }
                    val result = contactAsync.await()
                    tvThreadCoroutineAsyncResult.text = result
                }
            }
        }
    }

    class FetchContactAsyncTask(val listener: ContactAsyncTaskCallBack) :
        AsyncTask<URL, Int, String>() {

        // using WeakReference to Avoid Memory Leak in AsyncTask
        private val contactListener: WeakReference<ContactAsyncTaskCallBack> =
            WeakReference(listener)

        override fun onPreExecute() {
            super.onPreExecute()

            val myListener = contactListener.get()
            myListener?.onPreExecute()
        }

        override fun doInBackground(vararg urls: URL): String {
            val urlResult = urls[0].readText()
            return urlResult
        }

        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)

            val myListener = contactListener.get()
            myListener?.onProgressUpdate(*values)
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            val myListener = contactListener.get()
            myListener?.onPostExecute(result)
        }
    }

    override fun onPreExecute() {
        pbThreadAsyncProgress.visibility = View.VISIBLE
        tvThreadAsyncResult.visibility = View.GONE
    }

    override fun onProgressUpdate(vararg values: Int?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onPostExecute(result: String?) {
        pbThreadAsyncProgress.visibility = View.GONE
        tvThreadAsyncResult.text = result
        tvThreadAsyncResult.visibility = View.VISIBLE
    }
}

interface ContactAsyncTaskCallBack {
    fun onPreExecute()
    fun onProgressUpdate(vararg values: Int?)
    fun onPostExecute(result: String?)
}
