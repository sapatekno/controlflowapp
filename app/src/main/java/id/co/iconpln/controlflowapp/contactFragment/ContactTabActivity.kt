package id.co.iconpln.controlflowapp.contactFragment

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_contact_tab.*

class ContactTabActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_tab)

        setupTab()
    }

    private fun setupTab() {
        val contactTabPagerAdapter = ContactTabPagerAdapter(this, supportFragmentManager)
        vpContactTabContainer.adapter = contactTabPagerAdapter
        tlContactAdapter.setupWithViewPager(vpContactTabContainer)

        supportActionBar?.elevation = 0f
    }
}
