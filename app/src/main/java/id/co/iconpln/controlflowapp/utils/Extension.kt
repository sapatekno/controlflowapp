package id.co.iconpln.controlflowapp.utils

import androidx.appcompat.app.ActionBar

fun String.setActionBarTitle(supportActionBar: ActionBar?) {
    supportActionBar?.title = this
}