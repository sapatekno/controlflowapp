package id.co.iconpln.controlflowapp.database

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.loopj.android.http.AsyncHttpClient.log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class FavoriteViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: FavoriteDatabaseRepository

    private val allFavoriteUsers: LiveData<List<FavoriteUser>>

    init {
        val favDatabaseDao = FavoriteDatabase.getInstance(application).favoriteDatabaseDao
        repository = FavoriteDatabaseRepository(favDatabaseDao)
        allFavoriteUsers = repository.allFavUser
    }

    fun getAllFavoriteUsers(): LiveData<List<FavoriteUser>> {
        return allFavoriteUsers
    }

    fun insertUser(user: FavoriteUser) {
        GlobalScope.launch {
            repository.insertUser(user)
            log.d("favdatabase", "User ${user.userName} id ${user.userId} -  - INSERTED")
        }
    }

    fun deleteUser(userId: Int) {
        GlobalScope.launch {
            repository.deleteUser(userId)
            log.d("favdatabase", "User ${userId} -  - DELETED")
        }
    }

    fun getUser(id: Int): LiveData<FavoriteUser> {
        return repository.getuser(id)
    }

    fun updateUser(user: FavoriteUser) {
        GlobalScope.launch {
            repository.updateUser(user)
            log.d("favdatabase", "User ${user.userName} id ${user.userId} -  - UPDATED")
        }
    }
}