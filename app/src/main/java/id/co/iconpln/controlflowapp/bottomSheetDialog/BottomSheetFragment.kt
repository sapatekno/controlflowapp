package id.co.iconpln.controlflowapp.bottomSheetDialog


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.fragment_bottom_sheet.*

/**
 * A simple [Fragment] subclass.
 */
class BottomSheetFragment : BottomSheetDialogFragment(), View.OnClickListener {

    private var itemCLickListener: ItemCLickListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bottom_sheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setButtonClickListener()
    }

    private fun setButtonClickListener() {
        llBottomPreview.setOnClickListener(this)
        llBottomShare.setOnClickListener(this)
        llBottomEdit.setOnClickListener(this)
        llBottomSearch.setOnClickListener(this)
        llBottomExit.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.llBottomPreview -> {
                sendText(tvBottomPreview.text.toString())
            }
            R.id.llBottomShare -> {
                sendText(tvBottomShare.text.toString())
            }
            R.id.llBottomEdit -> {
                sendText(tvBottomEdit.text.toString())
            }
            R.id.llBottomSearch -> {
                sendText(tvBottomSearch.text.toString())
            }
            R.id.llBottomExit -> {
                sendText(tvBottomExit.text.toString())
            }
        }
    }

    private fun sendText(text: String) {
        if (itemCLickListener != null) itemCLickListener?.onItemClick(text)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is ItemCLickListener) {
            this.itemCLickListener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        this.itemCLickListener = null
    }

    interface ItemCLickListener {
        fun onItemClick(text: String)
    }
}
