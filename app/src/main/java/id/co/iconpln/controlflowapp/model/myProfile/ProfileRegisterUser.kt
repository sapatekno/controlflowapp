package id.co.iconpln.controlflowapp.model.myProfile

data class ProfileRegisterUser(
    val email: String,
    val name: String,
    val password: String,
    val phone: String
)