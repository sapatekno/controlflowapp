package id.co.iconpln.controlflowapp.myUserFavorite

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteUser
import kotlinx.android.synthetic.main.item_list_user.view.*

class MyUserFavoriteAdapter :
    RecyclerView.Adapter<MyUserFavoriteAdapter.MyUserFavoriteViewHodeler>() {

    private lateinit var onItemClickCallback: OnItemClickCallback
    private var myUserFavoriteData = emptyList<FavoriteUser>()

    inner class MyUserFavoriteViewHodeler(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(userItem: FavoriteUser) {
            itemView.tvUserName.text = userItem.userName
            itemView.tvUserAddress.text = userItem.userAddress
            itemView.tvUserMobile.text = userItem.userPhone
        }
    }

    fun setData(userItem: List<FavoriteUser>) {

        val listFavUser = ArrayList<FavoriteUser>()

        for (element in userItem) {
            listFavUser.add(element)
        }

        myUserFavoriteData = listFavUser
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyUserFavoriteViewHodeler {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_list_user, parent, false)
        return MyUserFavoriteViewHodeler(view)
    }

    override fun getItemCount(): Int {
        return myUserFavoriteData.size
    }

    override fun onBindViewHolder(holder: MyUserFavoriteViewHodeler, position: Int) {
        holder.bind(myUserFavoriteData[position])

        holder.itemView.setOnClickListener {
            onItemClickCallback.onItemClick(myUserFavoriteData[holder.adapterPosition])
        }
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    interface OnItemClickCallback {
        fun onItemClick(myUser: FavoriteUser)
    }
}