package id.co.iconpln.controlflowapp.myProfileRegister

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myProfile.ProfileRegisterUser
import kotlinx.android.synthetic.main.activity_my_profile_register.*

class MyProfileRegisterActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var viewModel: MyProfileRegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_profile_register)

        changeTitle()
        initViewModel()
        setButtonClickListener()
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(
            MyProfileRegisterViewModel::class.java
        )
    }

    private fun changeTitle() {
        supportActionBar?.title = "Register"
    }

    private fun setButtonClickListener() {
        btnProfileReg.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnProfileReg -> {
                fetchUserData(
                    ProfileRegisterUser(
                        etProfileRegEmail.text.toString().trim(),
                        etProfileRegName.text.toString().trim(),
                        etProfileRegPassword.text.toString().trim(),
                        etProfileRegHp.text.toString().trim()
                    )
                )
            }
        }
    }

    private fun fetchUserData(profileRegisterUser: ProfileRegisterUser) {
        viewModel.register(profileRegisterUser).observe(this, Observer { registerResponse ->
            if (registerResponse != null) {
                Toast.makeText(
                    this,
                    "Success Regiter ${registerResponse.email}",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }
}
