package id.co.iconpln.controlflowapp.sharedPreference

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_shared_preferences.*

class SharedPreferencesActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        private const val REQUEST_CODE = 100
    }

    private lateinit var userPreference: UserPreference

    private var isPreferenceEmpty = false

    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shared_preferences)
        setButtonClickListener()

        supportActionBar?.title = "My User Preference"
        userPreference = UserPreference(this)
        showExistingPreference()
    }

    private fun showExistingPreference() {
        user = userPreference.getUser()
        populateView(user)
        checkForm(user)
    }

    private fun checkForm(user: User) {
        when {
            user.name.toString().isNotEmpty() -> {
                btnPrefSave.text = resources.getText(R.string.sp_change)
                isPreferenceEmpty = false
            }
            else -> {
                btnPrefSave.text = resources.getText(R.string.sp_save)
                isPreferenceEmpty = true
            }
        }
    }

    private fun populateView(user: User) {
        tvPrefName.text = if (user.name.toString().isEmpty())
            "Tidak Ada" else user.name
        tvPrefAge.text = if (user.age.toString().isEmpty())
            "Tidak Ada" else user.age.toString()
        tvPrefEmail.text = if (user.email.toString().isEmpty())
            "Tidak Ada" else user.email
        tvPrefHandphone.text = if (user.handphone.toString().isEmpty())
            "Tidak Ada" else user.handphone
        tvPrefHobby.text = if (!user.hasReadingHobby)
            "Tidak Membaca" else "Membaca"
    }

    private fun setButtonClickListener() {
        btnPrefSave.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnPrefSave -> {
                val sharedPreferencesFormIntent =
                    Intent(this, SharedPreferencesFormActivity::class.java)

                when {
                    isPreferenceEmpty -> {
                        sharedPreferencesFormIntent.putExtra(
                            SharedPreferencesFormActivity.EXTRA_TYPE_FORM,
                            SharedPreferencesFormActivity.TYPE_ADD
                        )
                    }
                    else -> {
                        sharedPreferencesFormIntent.putExtra(
                            SharedPreferencesFormActivity.EXTRA_TYPE_FORM,
                            SharedPreferencesFormActivity.TYPE_EDIT
                        )
                    }
                }

                sharedPreferencesFormIntent.putExtra(SharedPreferencesFormActivity.EXTRA_USER, user)
                startActivityForResult(sharedPreferencesFormIntent, REQUEST_CODE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE) {
            if (resultCode == SharedPreferencesFormActivity.RESULT_CODE) {
                user = data?.getParcelableExtra(SharedPreferencesFormActivity.EXTRA_RESULT) as User
                populateView(user)
                checkForm(user)
            }
        }
    }
}
