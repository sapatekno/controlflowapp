package id.co.iconpln.controlflowapp.contact

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpClient.log
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import org.json.JSONObject

class ContactViewModel : ViewModel() {

    private val listContacts = MutableLiveData<ArrayList<Contact>>()

    internal fun setContact() {
        val client = AsyncHttpClient()
        val listItemContacts = ArrayList<Contact>()
        val url = "https://api.androidhive.info/contacts"

        // Request Contact API
        client.get(url, object : AsyncHttpResponseHandler() {
            override fun onSuccess(
                statusCode: Int,
                headers: Array<out Header>,
                responseBody: ByteArray
            ) {
                try {
                    // get data list JSON
                    val result = String(responseBody)
                    val responObject = JSONObject(result)
                    val arrayContacts = responObject.getJSONArray("contacts")

                    // convert JSON object to readable data
                    // data in JSON object is read by its KEY, exp: id, name
                    for (i in 0 until arrayContacts.length()) {
                        val contact = arrayContacts.getJSONObject(i)
                        val contactItem = Contact()

                        contactItem.id = contact.getString("id")
                        contactItem.name = contact.getString("name")
                        contactItem.email = contact.getString("email")
                        contactItem.mobile = contact.getJSONObject("phone").getString("mobile")

                        listItemContacts.add(contactItem)
                    }

                    // post realtime latest value from Background Thread
                    listContacts.postValue(listItemContacts)

                } catch (e: Exception) {
                    log.d("Exception", e.message.toString())
                }
            }

            override fun onFailure(
                statusCode: Int,
                headers: Array<out Header>,
                responseBody: ByteArray,
                error: Throwable
            ) {
                Log.d("onFailure", error.message.toString())
            }
        })
    }

    internal fun getContact(): LiveData<ArrayList<Contact>> {
        return listContacts
    }

}