package id.co.iconpln.controlflowapp.viewModel

import androidx.lifecycle.ViewModel

class VolumeViewModel : ViewModel() {
    var result: Double = 0.0

    fun doCalculate(length: String, width: String, height: String) {
        result = length.toDouble() * width.toDouble() * height.toDouble()
    }
}