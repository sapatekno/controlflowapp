package id.co.iconpln.controlflowapp

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.Menu
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_demo.*

class DemoActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo)
        Log.d("demo", "- - OnCreate")

        setButtonListener()
    }

    private fun setButtonListener() {
        btnSubmit.setOnClickListener(this)
        btnSnackbar.setOnClickListener(this)
        btnSnackbarButton.setOnClickListener(this)
        btnSnackbarCustom.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        Log.d("demo", "- - OnStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d("demo", "- - OnResume")
    }

    override fun onPause() {
        super.onPause()
        Log.d("demo", "- - OnPause")
    }

    override fun onStop() {
        super.onStop()
        Log.d("demo", "- - OnStop")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("demo", "- - OnRestart")
    }

    override fun onDestroy() {
        Log.d("demo", "- - OnDestroy")
        super.onDestroy()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnSubmit -> {
                val styleActivity = Intent(this, StyleActivity::class.java)
                startActivity(styleActivity)
            }
            R.id.btnSnackbar -> {
                Snackbar.make(clDemo, "This is simple Snackbar", Snackbar.LENGTH_SHORT).show()
            }
            R.id.btnSnackbarButton -> {
                Snackbar
                    .make(clDemo, "Message is deleted", Snackbar.LENGTH_LONG)
                    .setAction("Undo", undoListener)
                    .show()
            }
            R.id.btnSnackbarCustom -> {
                val customSnackbar = Snackbar
                    .make(clDemo, "This is a Custom Snackbar", Snackbar.LENGTH_LONG)
                    .setAction("Undo", undoListener)
                    .setActionTextColor(ContextCompat.getColor(this, R.color.button_snackbar))

                val snackbarView = customSnackbar.view
                snackbarView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))

                val textSnackbar: TextView = snackbarView.findViewById(R.id.snackbar_text)
                textSnackbar.setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
                textSnackbar.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)

                customSnackbar.show()
            }
        }
    }

    private val undoListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            Snackbar.make(clDemo, "Message is restored", Snackbar.LENGTH_SHORT).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
            .inflate(R.menu.menu_demo, menu)
        setupSearchView(menu)
        return super.onCreateOptionsMenu(menu)
    }

    private fun setupSearchView(menu: Menu?) {
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu?.findItem(R.id.action_demo_search)?.actionView as SearchView

        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.queryHint = getString(R.string.search_hint)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                Toast.makeText(this@DemoActivity, query, Toast.LENGTH_LONG).show()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
    }
}
