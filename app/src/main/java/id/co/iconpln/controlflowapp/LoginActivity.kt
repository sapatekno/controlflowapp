package id.co.iconpln.controlflowapp

import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_home.btnLogin
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setActionBarTitle("Form Login")
        etLoginEmail.requestFocus()
        btnLogin.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnLogin -> {
                doLogin()
            }
        }
    }

    private fun doLogin() {
        val email = etLoginEmail.text.toString()
        val password = etLoginPassword.text.toString()

        when {
            email.isBlank() -> etLoginEmail.error = "tidak boleh kosong"
            password.isBlank() -> etLoginPassword.error = "tidak boleh kosong"
            password.length < 7 -> etLoginPassword.error = "minimal password 7 karakter"
            !email.isEmailValid() -> etLoginEmail.error = "format email tidak benar"
            else -> {
                if (email == "user@mail.com" && password == "password") {
                    tvLoginStatus.text = getString(R.string.login_status_sukses)
                    tvLoginStatus.setTextColor(Color.GREEN)
                } else {
                    tvLoginStatus.text = getString(R.string.login_status_gagal)
                    tvLoginStatus.setTextColor(Color.RED)
                }
            }
        }
    }

    private fun String.isEmailValid(): Boolean {
        return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }

    private fun setActionBarTitle(title: String) {
        val actionBar = supportActionBar
        actionBar!!.title = title
    }
}
