package id.co.iconpln.controlflowapp

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import id.co.iconpln.controlflowapp.model.Person
import kotlinx.android.synthetic.main.activity_intent.*

class IntentActivity : AppCompatActivity(), View.OnClickListener {

    private val REQUEST_CODE = 110

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent)

        "Intent App".setActionBarTitle()
        setButtonClickListener()
    }

    private fun setButtonClickListener() {
        btn_intent_move_activity.setOnClickListener(this)
        btn_intent_move_activity_with_data.setOnClickListener(this)
        btn_intent_move_activity_with_bundle.setOnClickListener(this)
        btn_intent_move_activity_with_object.setOnClickListener(this)
        btn_intent_implicit.setOnClickListener(this)
        btn_intent_with_result.setOnClickListener(this)
        btn_open_web.setOnClickListener(this)
        btn_send_sms.setOnClickListener(this)
        btn_show_map.setOnClickListener(this)
        btn_share_text.setOnClickListener(this)
    }

    private fun String.setActionBarTitle() {
        val actionBar = supportActionBar
        actionBar?.title = this
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btn_intent_move_activity -> {
                val moveIntent = Intent(this, StyleActivity::class.java)
                startActivity(moveIntent)
            }
            R.id.btn_intent_move_activity_with_data -> {
                val dataActivity = Intent(this, IntentDataActivity::class.java)

                dataActivity.putExtra(IntentDataActivity.EXTRA_NAME, "Cucunguk")
                dataActivity.putExtra(IntentDataActivity.EXTRA_AGE, 20)

                startActivity(dataActivity)
            }
            R.id.btn_intent_move_activity_with_bundle -> {
                val bundleActivity = Intent(this, IntentBundleActivity::class.java)
                val bundle = Bundle()
                bundle.putString(IntentBundleActivity.EXTRA_BUNDLE_NAME, "Cucunguk")
                bundle.putInt(IntentBundleActivity.EXTRA_BUNDLE_AGE, 15)
                bundleActivity.putExtras(bundle)
                startActivity(bundleActivity)
            }
            R.id.btn_intent_move_activity_with_object -> {
                val person = Person("Kurir", 20, "Kurir@postman.com", "Yogyakarta")
                val objectActivity = Intent(this, IntentObjectActivity::class.java)
                objectActivity.putExtra(IntentObjectActivity.EXTRA_PERSON, person)
                startActivity(objectActivity)
            }
            R.id.btn_intent_implicit -> {
                val phoneNumber = "081232993121"
                val dialPhoneIntent = Intent(Intent.ACTION_DIAL, Uri.parse("tel: $phoneNumber"))

                // Check if user dont have receiver Intent Apps
                if (dialPhoneIntent.resolveActivity(packageManager) != null) {
                    startActivity(dialPhoneIntent)
                } else {
                    Toast.makeText(this, "You have no Phone App", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.btn_intent_with_result -> {
                val intentResultActivity = Intent(this, IntentResultActivity::class.java)
                startActivityForResult(intentResultActivity, REQUEST_CODE)
            }
            R.id.btn_open_web -> {
                val webPage = Uri.parse("http://www.binar.co.id")
                val openWebIntent = Intent(Intent.ACTION_VIEW, webPage)

                // Check if user dont have receiver Intent Apps
                if (openWebIntent.resolveActivity(packageManager) != null) {
                    startActivity(openWebIntent)
                } else {
                    Toast.makeText(this, "You have no Browser App", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.btn_send_sms -> {
                val phoneNumber = "081232123123"
                val sendSms = Uri.parse("smsto: $phoneNumber")
                val message = "Halo, siapa ini ?"
                val sendSmsIntent = Intent(Intent.ACTION_SENDTO, sendSms)
                sendSmsIntent.putExtra("sms_body", message)

                // Check if user dont have receiver Intent Apps
                if (sendSmsIntent.resolveActivity(packageManager) != null) {
                    startActivity(sendSmsIntent)
                } else {
                    Toast.makeText(this, "You have no Messaging App", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.btn_show_map -> {
                val latitude = "47.6"
                val longtitude = "-122.3"
                val location = Uri.parse("geo: $latitude, $longtitude")
                showMap(location)
            }
            R.id.btn_share_text -> {
                val sharedText = "Ini teks yang akan di lempar-lempar"
                val shareTextIntent = Intent(Intent.ACTION_SEND)
                shareTextIntent.putExtra(Intent.EXTRA_TEXT, sharedText)
                shareTextIntent.type = "text/plain"

                val shareIntent = Intent.createChooser(shareTextIntent, "Sebarkan Pesan")
                if (shareIntent.resolveActivity(packageManager) != null) {
                    startActivity(shareIntent)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_CODE -> {
                when (resultCode) {
                    IntentResultActivity.RESULT_CODE -> {
                        val selectedValue: Int? =
                            data?.getIntExtra(IntentResultActivity.EXTRA_VALUE, 0)
                        tev_intent_result.text = selectedValue.toString()
                    }
                }
            }
        }
    }

    private fun showMap(geoLocation: Uri) {
        val intent = Intent(Intent.ACTION_VIEW, geoLocation)

        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        } else {
            Toast.makeText(this, "You have no Map App", Toast.LENGTH_SHORT).show()
        }
    }
}
