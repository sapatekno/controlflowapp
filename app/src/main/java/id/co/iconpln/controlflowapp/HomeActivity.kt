package id.co.iconpln.controlflowapp

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import id.co.iconpln.controlflowapp.bottomSheetDialog.BottomSheetActivity
import id.co.iconpln.controlflowapp.contact.ContactActivity
import id.co.iconpln.controlflowapp.contactFragment.ContactTabActivity
import id.co.iconpln.controlflowapp.fragmentButtonNav.BottomNavActivity
import id.co.iconpln.controlflowapp.fragmentNavDrawer.NavDrawerActivity
import id.co.iconpln.controlflowapp.fragmentTab.TabActivity
import id.co.iconpln.controlflowapp.fragmentViewAdapter.ScrollActivity
import id.co.iconpln.controlflowapp.fragments.DemoFragmentActivity
import id.co.iconpln.controlflowapp.hero.GridHeroActivity
import id.co.iconpln.controlflowapp.hero.ListHeroActivity
import id.co.iconpln.controlflowapp.liveData.WeatherActivity
import id.co.iconpln.controlflowapp.myContact.MyContactActivity
import id.co.iconpln.controlflowapp.myProfile.MyProfileActivity
import id.co.iconpln.controlflowapp.myProfileLogin.MyProfileLoginActivity
import id.co.iconpln.controlflowapp.myUser.MyUserActivity
import id.co.iconpln.controlflowapp.sharedPreference.SharedPreferencesActivity
import id.co.iconpln.controlflowapp.thread.BackgroudThreadActivity
import id.co.iconpln.controlflowapp.utils.setActionBarTitle
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        "Home".setActionBarTitle(supportActionBar)
        setOnClickButton()
    }

    private fun setOnClickButton() {
        btnCalculation.setOnClickListener(this)
        btnClassification.setOnClickListener(this)
        btnLogin.setOnClickListener(this)
        btnOperation.setOnClickListener(this)
        btnStyle.setOnClickListener(this)
        btnDemo.setOnClickListener(this)
        btnVolume.setOnClickListener(this)
        btnIntent.setOnClickListener(this)
        btnComplexConstraint.setOnClickListener(this)
        btnConstraint.setOnClickListener(this)
        btnListHero.setOnClickListener(this)
        btnGridHero.setOnClickListener(this)
        btnDemoFragment.setOnClickListener(this)
        btnFragmentTab.setOnClickListener(this)
        btnBottomNav.setOnClickListener(this)
        btnNavDrawer.setOnClickListener(this)
        btnBottomSheetIntent.setOnClickListener(this)
        btnLocalizationIntent.setOnClickListener(this)
        btnScrollIntent.setOnClickListener(this)
        btnSharedPreferencesIntent.setOnClickListener(this)
        btnWeatherIntent.setOnClickListener(this)
        btnContactIntent.setOnClickListener(this)
        btnThreadIntent.setOnClickListener(this)
        btnContactTabIntent.setOnClickListener(this)
        btnMyContactIntent.setOnClickListener(this)
        btnMyUserIntent.setOnClickListener(this)
        btnMyProfileLoginIntent.setOnClickListener(this)
        btnMyProfileIntent.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnCalculation -> {
                val calculationIntent = Intent(this, MainActivity::class.java)
                startActivity(calculationIntent)
            }
            R.id.btnClassification -> {
                val classificationIntent = Intent(this, ClassificationActivity::class.java)
                startActivity(classificationIntent)
            }
            R.id.btnLogin -> {
                val classLoginIntent = Intent(this, LoginActivity::class.java)
                startActivity(classLoginIntent)
            }
            R.id.btnOperation -> {
                val operationIntent = Intent(this, OperationActivity::class.java)
                startActivity(operationIntent)
            }
            R.id.btnStyle -> {
                val styleIntent = Intent(this, StyleActivity::class.java)
                startActivity(styleIntent)
            }
            R.id.btnDemo -> {
                val demoIntent = Intent(this, DemoActivity::class.java)
                startActivity(demoIntent)
            }
            R.id.btnVolume -> {
                val volumeIntent = Intent(this, VolumeActivity::class.java)
                startActivity(volumeIntent)
            }
            R.id.btnIntent -> {
                val intentIntent = Intent(this, IntentActivity::class.java)
                startActivity(intentIntent)
            }
            R.id.btnComplexConstraint -> {
                val complexConstraintIntent = Intent(this, ComplexConstraintActivity::class.java)
                startActivity(complexConstraintIntent)
            }
            R.id.btnConstraint -> {
                val constraintIntent = Intent(this, ConstraintActivity::class.java)
                startActivity(constraintIntent)
            }
            R.id.btnListHero -> {
                val listHeroIntent = Intent(this, ListHeroActivity::class.java)
                startActivity(listHeroIntent)
            }
            R.id.btnGridHero -> {
                val gridHeroIntent = Intent(this, GridHeroActivity::class.java)
                startActivity(gridHeroIntent)
            }
            R.id.btnDemoFragment -> {
                val demoFragmentIntent = Intent(this, DemoFragmentActivity::class.java)
                startActivity(demoFragmentIntent)
            }
            R.id.btnFragmentTab -> {
                val tabIntent = Intent(this, TabActivity::class.java)
                startActivity(tabIntent)
            }
            R.id.btnBottomNav -> {
                val bottomNavIntent = Intent(this, BottomNavActivity::class.java)
                startActivity(bottomNavIntent)
            }
            R.id.btnNavDrawer -> {
                val navDrawerIntent = Intent(this, NavDrawerActivity::class.java)
                startActivity(navDrawerIntent)
            }
            R.id.btnBottomSheetIntent -> {
                val bottomSheetIntent = Intent(this, BottomSheetActivity::class.java)
                startActivity(bottomSheetIntent)
            }
            R.id.btnLocalizationIntent -> {
                val localizationIntent = Intent(this, LocalizationActivity::class.java)
                startActivity(localizationIntent)
            }
            R.id.btnScrollIntent -> {
                val scrollIntent = Intent(this, ScrollActivity::class.java)
                startActivity(scrollIntent)
            }
            R.id.btnSharedPreferencesIntent -> {
                val sharedPreferencesIntent = Intent(this, SharedPreferencesActivity::class.java)
                startActivity(sharedPreferencesIntent)
            }
            R.id.btnWeatherIntent -> {
                val weatherIntent = Intent(this, WeatherActivity::class.java)
                startActivity(weatherIntent)
            }
            R.id.btnContactIntent -> {
                val contactIntent = Intent(this, ContactActivity::class.java)
                startActivity(contactIntent)
            }
            R.id.btnThreadIntent -> {
                val backgroudThreadIntent = Intent(this, BackgroudThreadActivity::class.java)
                startActivity(backgroudThreadIntent)
            }
            R.id.btnContactTabIntent -> {
                val contactTabIntent = Intent(this, ContactTabActivity::class.java)
                startActivity(contactTabIntent)
            }
            R.id.btnMyContactIntent -> {
                val myContactIntent = Intent(this, MyContactActivity::class.java)
                startActivity(myContactIntent)
            }
            R.id.btnMyUserIntent -> {
                val myUserIntent = Intent(this, MyUserActivity::class.java)
                startActivity(myUserIntent)
            }
            R.id.btnMyProfileLoginIntent -> {
                val myProfileLoginIntent = Intent(this, MyProfileLoginActivity::class.java)
                startActivity(myProfileLoginIntent)
            }
            R.id.btnMyProfileIntent -> {
                val myProfileIntent = Intent(this, MyProfileActivity::class.java)
                startActivity(myProfileIntent)
            }
        }
    }
}
